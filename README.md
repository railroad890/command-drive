# command-drive
This repo is a basic command-based robot that uses one motor per drive side. Motor type will need to be changed for each specific robot.

## Future
I would like to add a chooser for control type, currently only two-joystick tank drive is implemented.

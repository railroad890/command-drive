package frc.robot.commands;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.DriveBaseSubsystem;

public class DefaultDriveCommand extends CommandBase {

    DriveBaseSubsystem driveBaseSubsystem;
    Joystick driverLeftStick;
    Joystick driverRightStick;

    public DefaultDriveCommand(DriveBaseSubsystem driveBaseSubsystem, Joystick driverLeftStick, Joystick driverRightStick)
    {
        this.driveBaseSubsystem = driveBaseSubsystem;
        this.driverLeftStick = driverLeftStick;
        this.driverRightStick = driverRightStick;

        addRequirements(driveBaseSubsystem);
    }

    @Override
    public void initialize()
    {
        
    }

    @Override
    public void execute()
    {
        this.driveBaseSubsystem.setLeft(-driverLeftStick.getRawAxis(1));
        this.driveBaseSubsystem.setRight(-driverRightStick.getRawAxis(1));
    }

    @Override
    public void end(boolean interupted)
    {

    }

    @Override
    public boolean isFinished()
    {
        return false;
    }
}

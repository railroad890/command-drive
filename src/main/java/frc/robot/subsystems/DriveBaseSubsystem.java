package frc.robot.subsystems;

import edu.wpi.first.wpilibj.motorcontrol.MotorController;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

//Subsystem controlling the driving, also estimates robot position
public class DriveBaseSubsystem extends SubsystemBase {
    
    private MotorController driveLeft;
    private MotorController driveRight;
    
    public DriveBaseSubsystem(MotorController driveLeft, MotorController driveRight)
    {
        this.driveLeft = driveLeft;
        this.driveRight = driveRight;
    }

    public void setLeft(double speed) //sets left wheel speed
    {
        this.driveLeft.set(speed);
    }
    
    public void setRight(double speed) //sets right wheel speed
    {
        this.driveRight.set(speed);
    }

    public void off()
    {
        this.driveLeft.set(0);
        this.driveRight.set(0);
    }

    protected void stop()
    {
        off();
    }
    
}